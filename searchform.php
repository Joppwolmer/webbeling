<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _e( 'Search for:', 'jointswp' ) ?></span>
		<input type="search" class="search-field" placeholder="<?php echo _e( 'Search...', 'webbeling' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo _e( 'Search for:', 'webbeling' ) ?>" />
	</label>
	<input type="submit" class="search-submit button" value="<?php echo _e( 'Search', 'webbeling' ) ?>" />
</form>