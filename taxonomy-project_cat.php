<?php get_header(); ?>

	<div id="content">

		<div class="inner">

		    <main id="main" role="main">

                <?php get_template_part( 'partials/project', 'project' ); ?>

		    </main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>