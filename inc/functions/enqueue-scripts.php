<?php
function site_scripts() {
    global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    $version = time();

    // MAIN SCRIPTS
    wp_enqueue_script( 'webbeling-script', get_template_directory_uri() . '/assets/stage/scripts/webbeling-script.js', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'webbeling-views', get_template_directory_uri() . '/assets/stage/scripts/webbeling-views.js', array( 'jquery' ), $version, true );

    //FONT
    wp_enqueue_style( 'font', 'https://use.typekit.net/bwe5bck.css' , array(), $version, 'all' );
    
    // MAIN CSS
    wp_enqueue_style( 'webbeling-css', get_template_directory_uri() . '/assets/stage/css/webbeling.css', array(), $version, 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

}

add_action('wp_enqueue_scripts', 'site_scripts', 999);