<?php
	
// Adding WP Functions & Theme Support
function webbeling_theme_support() {

	// Add WP Thumbnail Support
	add_theme_support( 'post-thumbnails' );

	//Add custom menus
	add_theme_support( 'menus' );

	// Add custom logo support
	add_theme_support( 'custom-logo' );

	// Add custom header support
	add_theme_support( 'custom-header' );
	
	// Default thumbnail size
	set_post_thumbnail_size(125, 125, true);

	// Add RSS Support
	add_theme_support( 'automatic-feed-links' );
	
	// Add Support for WP Controlled Title Tag
	add_theme_support( 'title-tag' );
	
	// Add HTML5 Support
	add_theme_support( 'html5', 
	         array( 
	         	'comment-list', 
	         	'comment-form', 
	         	'search-form', 
	         ) 
	);
	
	// Adding post format support
	 add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	); 
	
	// Set the maximum allowed width for any content in the theme, like oEmbeds and images added to posts.
	$GLOBALS['content_width'] = apply_filters( 'webbeling_theme_support', 1200 );

	//REMOVE P & BR-TAGS
	remove_filter( 'the_content', 'wpautop' );
    remove_filter( 'the_excerpt', 'wpautop' );

	
} /* end theme support */

add_action( 'after_setup_theme', 'webbeling_theme_support' );


function my_custom_upload_mimes($mimes = array()) {

	// Add a key and value for the CSV file type
	$mimes['ogv'] = "video/ogv";

	return $mimes;
}

add_action('upload_mimes', 'my_custom_upload_mimes');
