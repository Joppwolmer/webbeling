<?php
// Comment Layout
function joints_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class('panel'); ?>>
        <article id="comment-<?php comment_ID(); ?>">
            <header class="comment-meta">
                <span class="author">
                    <?php echo get_avatar( $comment, 75 ); ?>
                    <span><?php printf(__('%s', 'webbeling'), get_comment_author_link()) ?></span>
                </span>
                <span class="date">
                    <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><time datetime="<?php echo comment_time('j M, H:i'); ?>"><?php echo comment_time('j M, H:i'); ?></time></a>
                </span>
            </header>
            <?php edit_comment_link(__('(Edit)', 'webbeling'),'  ','') ?>
            <?php if ($comment->comment_approved == '0') : ?>
                <div class="alert alert-info">
                    <p><?php _e('Your comment is awaiting moderation.', 'webbeling') ?></p>
                </div>
            <?php endif; ?>
            <section class="comment-content">
                <?php comment_text() ?>
            </section>
		</article>
        <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	<!-- </li> is added by WordPress automatically -->
<?php
}

