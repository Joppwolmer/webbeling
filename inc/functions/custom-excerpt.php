<?php

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
// Replaces the excerpt "Read More" text by a link
add_filter('excerpt_more', 'new_excerpt_more', 100);
function new_excerpt_more($more) {
   return '... <a class="read-more text-button" href="'. get_permalink( get_the_ID() ) . '">' . __('Read more', 'webbeling') . '</a>';
}


?>