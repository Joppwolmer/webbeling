<?php

/************* PORTFOLIO INIT *************/

function project_init(){

    //PROJECT IMAGE
	add_image_size( 'project-featured-img', 900, 900, true );

    //INIT
	project_register();

}
add_action('init', 'project_init');



/**
 * MENU FIX
 *
 *
 */
function custom_menu_item_classes($classes = array(), $menu_item = false){

	// use this format for adding highlighting
    if((is_singular('project') || is_post_type_archive('project')) && $menu_item->ID == 1959) {
		$classes[] = 'current-menu-item';
	}

	return $classes;
}
add_filter( 'nav_menu_css_class', 'custom_menu_item_classes', 10, 2 );


/**
 * Adding custom post type portfolio
 *
 *
 */
function project_register() {


	$labels = array(
		'name' => __('Projects', 'webbeling'),
		'singular_name' => __('Project item', 'webbeling'),
		'add_new' => __('Add new', 'webbeling'),
		'add_new_item' => __('Add new project', 'webbeling'),
		'edit_item' => __('Edit', 'webbeling'),
		'new_item' => __('New', 'webbeling'),
		'view_item' => __('View project', 'webbeling'),
		'search_items' => __('Search project', 'webbeling'),
		'not_found' =>  __('Nothing found', 'webbeling'),
		'not_found_in_trash' => __('Nothing found in trash', 'webbeling'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_ui' => true,
        'query_var' => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'rewrite' => array( 'slug' => 'projekt' ),
		'supports' => array('title','editor', 'author', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes'),
		'show_in_rest' => true
	  );

	register_post_type( 'project' , $args );

    // now let's add custom categories (these act like categories)
    register_taxonomy( 'project_cat',
        array('project'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
        array('hierarchical' => true,     /* if this is true, it acts like categories */
            'labels' => array(
                'name' => __( 'Project Categories', 'webbeling' ), /* name of the custom taxonomy */
                'singular_name' => __( 'Project Category', 'webbeling' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Project Categories', 'webbeling' ), /* search title for taxomony */
                'all_items' => __( 'All Project Categories', 'webbeling' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent Project Category', 'webbeling' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent Project Category:', 'webbeling' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit Project Category', 'webbeling' ), /* edit custom taxonomy title */
                'update_item' => __( 'Update Project Category', 'webbeling' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New Project Category', 'webbeling' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New Project Category Name', 'webbeling' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'project-category' ),
        )
    );

}


/************* CUSTOM FIELDS *************/

/*add_action( 'add_meta_boxes', 'add_project_metaboxes' );

//Add meta boxes

function add_project_metaboxes() {
	add_meta_box('project_meta', __('Meta', 'webbeling'), 'project_meta_output', 'project', 'advanced', 'core');
}

// Output Meta HTML

function project_meta_output($post) {

	echo '<input type="hidden" name="projectmeta_noncename" id="projectmeta_noncename" value="' .
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	$defending_text = get_post_meta($post->ID, 'project_meta', true);
	wp_editor($defending_text, 'project_meta_editor', array('textarea_name' => 'project_meta' ));
}

// Save the Metabox Data

function project_meta_save($post_id, $post) {

	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['projectmeta_noncename'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though.
	$project_meta['project_meta'] = $_POST['project_meta'];

	// Add values of $events_meta as custom fields

	foreach ($project_meta as $key => $value) { // Cycle through the $position_meta array!
		if( $post->post_type == 'revision' ) return; // Don't store custom data twice
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
	}

	global $wpdb;
  	$wpdb->update( $wpdb->posts, array( 'ID' => $post_id ) );

}

add_action('save_post', 'project_meta_save', 1, 2); // save the custom fields
*/

?>