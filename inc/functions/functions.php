<?php

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

// Theme support options
require_once(get_template_directory().'/inc/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/inc/functions/cleanup.php');

// Register scripts
require_once(get_template_directory().'/inc/functions/enqueue-scripts.php');

 // Register article meta function
 require_once(get_template_directory().'/inc/functions/article-meta.php');

// Register article meta function
require_once(get_template_directory().'/inc/functions/custom-excerpt.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/inc/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/inc/functions/comments.php');

// Moving the Comment Text Field to Bottom
require_once(get_template_directory().'/inc/functions/comment-form.php');

// MENUS
require_once(get_template_directory().'/inc/functions/menu.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/inc/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory().'/inc/functions/translation.php');

// Related post function - no need to rely on plugins
require_once(get_template_directory().'/inc/functions/related-posts.php');

// Related post function - no need to rely on plugins
require_once(get_template_directory().'/inc/functions/breadcrumbs.php');

// Adds site styles to the WordPress editor
require_once(get_template_directory().'/inc/functions/editor-styles.php');

// Adds project custom post type
require_once(get_template_directory().'/inc/functions/project-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php');

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php');

// Adds people custom post type
//require_once(get_template_directory().'/inc/functions/custom-post-type.php');

?>