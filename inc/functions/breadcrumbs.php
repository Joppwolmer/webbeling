<?php

/**
 * Get taxonomies terms links.
 *
 * @see get_object_taxonomies()
 */
function get_first_custom_taxonomies() {
    // Get post by post ID.
    if ( ! $post = get_post() ) {
        return '';
    }

    // Get post type by post.
    $post_type = $post->post_type;

    // Get post type taxonomies.
    $taxonomies = get_object_taxonomies( $post_type, 'objects' );

    $out = array();

    foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){

        // Get the terms related to post.
        $terms = get_the_terms( $post->ID, $taxonomy_slug );

        if ( ! empty( $terms ) ) {
            foreach ( $terms as $term ) {
                return (object) [
                    'name' => esc_html( $term->name ),
                    'url' => esc_url( get_term_link( $term->slug, $taxonomy_slug ) ),
                ];
            }
        }
    }
}

function get_custom_post_slug(){
    $post_type = get_post_type();
    if ( $post_type )
    {
        $post_type_data = get_post_type_object( $post_type );
        $post_type_slug = $post_type_data->rewrite['slug'];

        return $post_type_slug;
    }
}



function the_breadcrumb() {
    if (!is_front_page()) {

	    $breadcrumbs = '';

        global $post;

	    //FIRST
        $breadcrumbs = '<li class="first"><a href="' . get_bloginfo('siteurl') . '">' . get_bloginfo('name') . '</a></li>';

	    //BLOG PAGE
	    if('post' == get_post_type()){
            $breadcrumbs .= "<li><a href=".get_permalink( get_option( 'page_for_posts' ) ).">".get_the_title( get_option('page_for_posts', true) )."</a></li>";
	    }

        //BLOG CATEGORY & SINGLE
        if (is_category() || is_single() && get_post_type() != 'project' ){

            //CATEGORY
            $categories = get_the_category();
            if ( ! empty( $categories ) ) {
                $breadcrumbs .= '<li><a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a></li>';
            }

            //SINGLE
            if (is_single()){
                $breadcrumbs .= '<li><a href="'.get_the_permalink($post).'">'.get_the_title( $post ).'</a></li>';
            }

        }

        // BLOG ARCHIVE
        if (!is_category() && is_archive() && get_post_type() != 'project' ){
            $breadcrumbs .= '<li class="last">' . get_the_archive_title() . '</li>';
        }

        // PROJECT SINGLE
        if ('project' == get_post_type()) {

            $project_type = get_post_type_object( 'project' );
            $project_url = $project_type->rewrite['slug'];
            $project_label = $project_type->label;
            $breadcrumbs .= "<li><a href='$url/$project_url'>$project_label</a></li>";

            if(is_single()){
                $list = get_the_terms(get_the_ID(), 'project_cat');
                $project_type = get_post_type_object( 'project' );

                $url = get_site_url();

                $item = $list[0];
                $project_url = $project_type->rewrite['slug'];
                $project_label = $project_type->label;

                $breadcrumbs .= "<li><a href='$url/$project_url?t=$item->name'>$item->name</a></li>";

                $breadcrumbs .= '<li class="last">' . get_the_title() . '</li>';
            }
        }

        // PAGE
        if (is_page()) {

            $current_page .= '<li class="last">' . $post->post_title . '</li>';

            while($post->post_parent != ''){
                $post = get_post($post->post_parent);
                $breadcrumbs .= '<li><a href="' . get_permalink($post->ID) . '">' . $post->post_title . '</a></li>';
            }

            $breadcrumbs .= $current_page;

        }


        echo '<ul class="breadcrumbs-list">' . $breadcrumbs . '</ul>';

    }
}
?>