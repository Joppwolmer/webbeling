<?php
/*
webbeling language load
*/

// Adding Translation Option
function load_translations(){

	load_theme_textdomain( 'webbeling', get_template_directory() .'/translation' );

    $locale = get_locale();
    $locale_file = get_template_directory() . "/languages/$locale.php";

    if ( is_readable( $locale_file ) ) {
        require_once( $locale_file );
    }

}

add_action('after_setup_theme', 'load_translations');

?>