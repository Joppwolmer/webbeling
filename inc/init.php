<?php
/**
 * Webbeling engine room
 *
 * @package webbeling2
 */

/**
 * Initialize all theme things.
 */
require get_template_directory() . '/inc/functions/functions.php';

/**
 * Initialize Woocommerce things.
 */
require get_template_directory() . '/inc/woocommerce/init.php';



