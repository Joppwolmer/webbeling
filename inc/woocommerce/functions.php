<?php

/**
 * GENERAL
 */

/**
 * Add 'woocommerce-active' class to the body tag
 * @param  array $classes
 * @return array $classes modified to include 'woocommerce-active' class
 */
function webbeling_woocommerce_body_class( $classes ) {
	if ( is_woocommerce_activated() ) {
		$classes[] = 'woocommerce-active';
	}

	return $classes;
}


/**
 * Cart Link
 * Displayed a link to the cart including the number of items present and the cart total
 *
 * @return void
 * @since  1.0.0
 */
function header_cart() {
	if ( is_woocommerce_activated() ) {
		?>
			<a class="cart-contents" id="cart-button" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'webbeling' ); ?>">
				<span class="icon"></span>
				<?php /* translators: %d: number of items in cart */ ?>
				<?php echo wp_kses_post( WC()->cart->get_cart_subtotal() ); ?> <span class="count"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() ) ?></span>
			</a>
		<?php
	}
}

/**
 * Mini Cart
 * Displayed mini cart
 *
 * @return void
 * @since  1.0.0
 */
function mini_cart() {
	if ( is_woocommerce_activated() ) {
		?>
			<div id="mini-cart">
				<header id="min-cart-header">
					<h2><?php echo _e('Products', 'webbeling'); ?></h2>
					<span id="mini-cart-close"></span>
				</header>
				<?php woocommerce_mini_cart(); ?>
			</div>
		<?php
	}
}



/**
 * CATEGORY
 */


/**
 * Display category image on category archive
 */
function custom_category_image() {
    if ( is_product_category() ){
	    global $wp_query;
	    $cat = $wp_query->get_queried_object();
	    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
	    $image = wp_get_attachment_image( $thumbnail_id, "large");
	    if ( $image ) {
		    echo '<div class="category-image">'.$image.'</div>';
		}
	}
}

/**
 * Wrap ordering and count html
 */
function custom_ordering_count_wrap() {
    echo '<div class="count-ordering-wrapper">';
    woocommerce_result_count();
    woocommerce_catalog_ordering();
    echo '</div>';
}



/**
 * PRODUCT
 */


 /**
  * Returns the product rating in html format.
  *
  * @param string $rating (default: '')
  *
  * @return string
  */
 function custom_get_rating_html($rating_html, $rating ) {
 	$rating_html = '';
 	if ( $rating > 0 ) {
 	    $rating_html  = '<div class="star-rating" title="' . sprintf( __( 'Rated %s out of 5', 'woocommerce' ), $rating ) . '"><a href="#tab-title-reviews">';
 	    for($i=0; $i<5; $i++){
             if( $i < $rating ){
 	            $rating_html .= '<span class="selected"></span>';
             }else{
                 $rating_html .= '<span></span>';
 	        }
 	    }
 	    $rating_html .= '</a></div>';
 	}else{
 		$rating_html = "<div class='star-rating'></div>";
 	}

 	return $rating_html;
 }



 /**
 * CHECKOUT
 */

  /**
  * Returns header for payment
  * @return string
  */
  function custom_payment_header() {
	echo '<h3 class="payment-header">';
	echo __( 'Payment method', 'webbeling' );
	echo '</h3>';
}