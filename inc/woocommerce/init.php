<?php

/**
 * Query WooCommerce activation
 */
if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	function is_woocommerce_activated() {
		return class_exists( 'woocommerce' ) ? true : false;
	}
}


/**
 * Load WooCommerce compatibility files.
 */
if ( is_woocommerce_activated() ) {

    // Declare WooCommerce support
    add_theme_support( 'woocommerce', array(
        'thumbnail_image_width' => 400,
        'gallery_thumbnail_image_width' => 900,
        'single_image_width' => 900,
        'product_grid'          => array(
            'default_rows'    => 5,
            'min_rows'        => 3,
            'max_rows'        => 7,
            'default_columns' => 4,
            'min_columns'     => 4,
            'max_columns'     => 4,
        ),
    ) );

    //Category image crop option
    update_option( 'woocommerce_thumbnail_cropping', '16:10' );

    //add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    //add_theme_support( 'wc-product-gallery-slider' );

    // Declare support for title theme feature
    add_theme_support( 'title-tag' );

}

require get_template_directory() . '/inc/woocommerce/hooks.php';
require get_template_directory() . '/inc/woocommerce/functions.php';
require get_template_directory() . '/inc/woocommerce/template-tags.php';
require get_template_directory() . '/inc/woocommerce/integrations.php';