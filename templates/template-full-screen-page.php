<?php
/*
Template Name: Full Screen Page
*/
?>

<?php get_header(); ?>

	<div id="content">

		<div class="full-screen">

		    <main id="main" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'partials/page', 'header-full' ); ?>

					<?php get_template_part( 'partials/page', 'content' ); ?>

				<?php endwhile; endif; ?>

			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
