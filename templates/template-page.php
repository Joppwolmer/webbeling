<?php
/*
Template Name: Page with background
*/
?>

<?php get_header(); ?>

	<div id="content" class="page-background">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php get_template_part( 'partials/loop', 'page' ); ?>

        <?php endwhile; endif; ?>

	</div> <!-- end #content -->

<?php get_footer(); ?>
