<?php
/*
Template Name: Page Text
*/
?>

<?php get_header(); ?>

	<div id="content">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php get_template_part( 'partials/page', 'header-full' ); ?>
			
			<div class="inner content-text">

				<main id="main" role="main">

						<?php get_template_part( 'partials/page', 'content' ); ?>

				</main> <!-- end #main -->

			</div> <!-- end #inner-content -->

		<?php endwhile; endif; ?>

	</div> <!-- end #content -->

<?php get_footer(); ?>
