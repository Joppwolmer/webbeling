<!doctype html>

<html class="no-js" <?php language_attributes(); ?>>

<head>
	<meta charset="utf-8">

	<!-- Force IE to use the latest rendering engine available -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Mobile Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta class="foundation-mq">

	<!-- If Site Icon isn't set in customizer -->
	<?php if (!function_exists('has_site_icon') || !has_site_icon()) { ?>
		<!-- Icons & Favicons -->
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png?v=2">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico?v=2">
		<![endif]-->

		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
		<meta name="theme-color" content="#000000">
	<?php } ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>

	<?php get_template_part('partials/head', 'head'); ?>

</head>

<body <?php body_class(); ?>>

	<div id="main-wrapper" <?php if (is_active_sidebar('top-bar')) : ?>class="top-bar-active" <?php endif; ?>>

		<?php get_template_part('partials/header/header', 'header'); ?>

		<div id="content-wrapper">

			<?php get_template_part('partials/breadcrumbs', 'breadcrumbs'); ?>