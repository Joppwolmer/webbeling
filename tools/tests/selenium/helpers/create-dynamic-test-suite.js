exports.create = function (activeModules) {

    var grunt = require("grunt");
    var existingCaseList = [];
    var modulesCaseList = [];
    var caseList = [];
    var fileContent = "";

    function createFileList(path, callback) {
        grunt.file.recurse(path, callback);
    }

    function createExistingCaseList(abspath, rootdir, subdir, filename) {
        existingCaseList.push(filename);
    }

    // Create a list with all existing cases in folder tools/tests/selenium/src
    createFileList("tools/tests/selenium/src", createExistingCaseList);
    for (var i = 0; i < existingCaseList.length; i++) {
        var obj = existingCaseList[i];
        var str = '<tr><td><a href="src/' + existingCaseList[i] + '">' + existingCaseList[i] + '</a></td></tr>';
        caseList.push(str);
    }

    // Add tests from active modules
    for (i = 0; i < activeModules.active.length; i++) {
        var modulePath = "assets/modules/" + activeModules.active[i] + "/tests/" + activeModules.active[i] + ".html";
        if (grunt.file.exists(modulePath)) {
            var str = '<tr><td><a href="../../../'+ modulePath + '">' + activeModules.active[i] + '</a></td></tr>';
            caseList.push(str);
        }
    }

    // Create file content
    fileContent += '<?xml version="1.0" encoding="UTF-8"?>\n';
    fileContent += '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n';
    fileContent += '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">\n';
    fileContent += '    <head>\n';
    fileContent += '    <meta content="text/html; charset=UTF-8" http-equiv="content-type" />\n';
    fileContent += '    <title>Test Suite -  Full</title>\n';
    fileContent += '    <link rel="selenium.base" href="http://foundation2.jetshop.se/" />\n';
    fileContent += '</head>\n';
    fileContent += '<body>\n';
    fileContent += '<table id="suiteTable" cellpadding="1" cellspacing="1" border="1" class="selenium"><tbody>\n';
    fileContent += '    <tr><td><b>Test Suite</b></td></tr>\n';
    for (var i = 0; i < caseList.length; i++) {
        fileContent += caseList[i] + "\n";
    }
    fileContent += '<tr><td><a href="helpers/test-complete.html">test-complete.html</a></td></tr>\n';
    fileContent += '</tbody></table>\n';
    fileContent += '</body>\n';
    fileContent += '</html>\n';

    return fileContent;
};
