//
//  This file will extract values from SASS-variables in _base-settings.scss
//  and prepend to file "base-responsive-modules.js".
//
//  Add SASS-selectors to the whiteList array to extract additional values.
//
module.exports = function () {
    var fs = require("fs");
    var breakPoints = {};
    var str = "";
    var whiteList = [
        {
            "label": "small",
            "key": "$small-breakpoint"
        },
        {
            "label": "medium",
            "key": "$medium-breakpoint"
        },
        {
            "label": "large",
            "key": "$large-breakpoint"
        },
        {
            "label": "xlarge",
            "key": "$xlarge-breakpoint"
        }
    ];
    var content = fs.readFileSync("assets/scss/_base_settings.scss", 'utf-8');
    var lines = content.split('\n');
    for (var line = 0; line < lines.length; line++) {
        var row = lines[line];
        var key = row.substring(0, row.indexOf(':'));
        var value = row.substring(row.indexOf(':'), row.length).trim();
        value = value.replace(/^:/, '').replace(/^ /, '').replace(/\;$/, '');
        if ((key.length > 0) && (key.search("//") != 0)) {
            for (var j in whiteList) {
                if (whiteList[j]["key"] ==key) {
                    breakPoints[[whiteList[j]["label"]]] = parseInt( value.replace("em-calc(","").replace(")","").replace("px","") );
                }
            }
        }
    }
    str += "// Inserted by build-process, getSassBreakpoints()\n";
    str += "J.config.breakpoints = " + JSON.stringify(breakPoints) + ";";
    return str;
};
