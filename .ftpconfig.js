var gutil = require("gulp-util");

var config = {
  host: "ftp-host",
  user: "username",
  password: "password",
  ftpPath: "public_html/wp-content/themes/webbeling", // Prod Path on FTP server
  parallel: 1,
  maxConnections: 1,
  //"log": gutil.log,                     // Enable this for more verbose logging
  port: 21, // Use port number 21, not 990
  secure: false, // Leave security settings intact
  secureOptions: {
    rejectUnauthorized: false,
  },
};

module.exports = config;
