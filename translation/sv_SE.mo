��    o      �  �         `	     a	     o	     |	     	     �	     �	     �	     �	     �	     �	     �	      
     
     
     .
     ?
     O
     f
     m
     s
     {
     �
     �
     �
     �
  (   �
  
   �
     �
     
               1     A     Q     g     m     �     �     �     �  a   �  	              %     *  	   3     =     A     Z     n     |     �     �     �     �     �     �               $     ;     S     e     x     �     �     �     �     �     �     �     �     
             	   2     <     U     \     u     �     �     �     �     �  	   �     �     �                 ,   !     N  L   [  D   �  $   �            8   1     j     �     �     �     �     �     �  9   �  $   .  )   S     }  G  �     �     �     �  
   �             
        )     E     ]     w     �     �     �     �     �     �            
     
   '     2     D     T     b  (   r  	   �     �     �     �     �     �     �          *  #   1     U  
   ]     h     n  b   �  	   �     �     �     �                    4     L     [     w     ~  $   �     �  !   �     �          !     2     I     a     t     �     �  
   �     �     �     �  	   �     �     
          "     *     <     E     [     `     |     �     �     �     �     �     �  	             '     .     6  ,   C  
   p  h   {  7   �  (        E     M  8   ^     �     �     �     �     �  
          ?     $   Y  -   ~     �     f   '   (   +       S   J   k       <   M       V   b           ^      R       2   Q       &   1   #   a              A          "           >   -      C           o   H      ;       4   !   l       %   	   
              \   B   D   5   U      d   m   Z       F   ]                           e   8   :       O   g          6   *   /   =       )   j       i   .   L   7   _   0   Y         P      G          W   E       c                     n           9      `   K      T         ,          I              N      h   X      [      $   3   @       ?               %1$s comments %1$s on %2$s %s (Edit) ,  <h4>Related Posts</h4> Add New Add New Custom Category Add New Custom Tag Add New Custom Type Add New Project Category Add new Add new project All Custom Categories All Custom Posts All Custom Tags All Project Categories Cancel Close Comment Comment navigation Comment on %s Custom Categories Custom Category Custom Post Custom RSS Feed (Customize in admin.php) Custom Tag Custom Tags Custom Types Edit Edit Custom Category Edit Custom Tag Edit Post Types Edit Project Category Email Epic 404 - Article Not Found First Footer Last Left area on mobile nav Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Main Menu Menu Name Nav-Left Nav-Right New New Custom Category Name New Custom Tag Name New Post Type New Project Category Name Next No comments Nothing found Nothing found in Trash Nothing found in the Database. Nothing found in trash One comment Oops, Post Not Found! Parent Custom Category Parent Custom Category: Parent Custom Tag Parent Custom Tag: Parent Project Category Parent Project Category: Payment method Permalink to %s Posts by %s Previous Products Project Categories Project Category Project item Projects Rated %s out of 5 Read more Right area on mobile nav Search Search Custom Categories Search Custom Tags Search Post Type Search Project Categories Search Results for: Search for: Search project Search... Show all Sorry, No Results. Sticky Tags: The Footer. The RSS Feed is either empty or unavailable. The Top-bar. The article you were looking for was not found, but maybe try looking again! This is the error message in the parts/content-missing.php template. This is the example custom post type Top-bar Try your search again. Uh Oh. Something is missing. Try double checking things. Update Custom Category Update Custom Tag Update Project Category View Post Type View all posts by %s View project View your shopping cart You must be <a href="%s">logged in</a> to post a comment. Your comment is awaiting moderation. Your email address will not be published. j F Y @ g:i a Project-Id-Version: webbeling
PO-Revision-Date: 2020-02-03 14:16+0100
Last-Translator: 
Language-Team: Ebeling Webbyrå AB
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: ;__;_e;esc_attr_e;_nx;_x;esc_html_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: partials
X-Poedit-SearchPathExcluded-0: node_modules
X-Poedit-SearchPathExcluded-1: tools
X-Poedit-SearchPathExcluded-2: .git
 %1$s kommentarer %1$s på %2$s %s (Redigera) , <h4>Relaterade inlägg</h4> Lägg till Lägg till ny egen kategori Lägg till ny egen tagg Lägg till ny inläggstyp Lägg till ny projektkategori Lägg till nytt Lägg till nytt projekt Alla egna kategorier Alla specialinlägg Alla egna taggar Alla projektkategorier Avbryt Stäng Kommentera Navigation Kommentera på %s Egna kategorier Egen kategori Special inlägg Anpassa RSS flöde (Anpassa i admin.php) Egen tagg Special taggar Special inläggstyp Redigera Redigera egen kategori Editera egen tagg Redigera inläggstyper Redigera projektkategori E-post Oj! Sidan kunde tyvärr inte hittas Första Footer-yta Sista Vänstersida mobil nav. Inloggad som <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Logga ut?</a> Huvudmeny Meny Namn Nav. vänster Nav. höger Ny Namn på ny egen kategori Nytt namn på egen tagg Ny inläggstyp Namn på ny projektkategori Nästa Inga kommentarer Ingenting kan hittas i papperskorgen Inget hittades i papperskorgen Ingenting kan hittas i databasen. Inget hittades i papperskorgen En kommentar Kan inte hittas! Nedärvd egen kategori Nedärvd egen kategori: Nedärvd egen tagg Nedärvd egen tagg: Moder projektkategorier Projektkategorier: Betalmetod Permalänk till %s Inlägg av %s Tidigare Produkter Kategoriserade inlägg: Projektkategori Projekt Projekt Betygsatt %s av 5 Läs mer Högersida mobil nav. Sök Sök i de egna kategorierna Sök egna taggar Sök inläggstyp Sök i projektkategorierna Sök efter resultat för: Sök på sajten... Sök efter: Sök på sajten... Visa alla Tyvärr, inga resultat. Sticky Taggar: Footer-ytan. RSS flödet är tomt eller ej tillgängligt. Top-bar:en Sidan du försökte besöka kunde inte hittas. Antingen är adressen fel eller så har den tagits bort.  Felmeddelande från parts/content-missing.php template. Det här är ett exempel på inläggstyp Top-bar Prova sök igen. Uh Oh. Något gick fel. Prova att dubbelkolla allt igen. Uppdatera egen kategori Uppdatera egen tagg Uppdatera projektkategori Se inläggstyp Se alla inlägg av %s Se projekt Se varukorg Du behöver vara <a href="%s">inloggad</a> för att kommentera. Din kommentar inväntar godkännande Din e-post adress kommer inte att publiceras. j F Y @ g:i a 