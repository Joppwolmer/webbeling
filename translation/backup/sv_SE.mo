��    o      �  �         `	     a	     n	     q	     x	     {	     �	  ^   �	     
     
     '
     :
     N
     g
     o
     
     �
     �
     �
     �
     �
     �
     �
     �
       (     
   9     D     P     ]     b     w     �     �     �     �     �     �     �     �     �  b   �  
   ^     i     {     �     �     �     �     �     �     �     �     �     �          ,     C     Y     p     �     �     �     �     �     �     �               '     4     =     B     O     \     c     |     �     �     �     �     �  	   �     �     �                    $     0  ,   >     k  L   x     �  D   �  $   '     L     T  8   k     �     �     �     �     �     	       9     $   R  )   w     �     �    �     �     �  
   �     �     �     �  `     
   p     {     �     �     �  
   �     �     
          3     D     [     b  
   i     t     �     �     �  	   �     �     �     �     �                (     A  #   H  !   l     �     �  
   �     �  b   �  
        %     7     =     B     G     J     d     |     �     �  $   �  $   �  !   �  $        ?     P     g          �     �     �     �     �     �     �          #     +  	   3     =     I     U     Z     v     �     �     �     �     �     �  	   �               #     *     2  	   ?  +   I  
   u  9   �     �  7   �  (        /     7  8   H     �     �     �     �     �  
   �     �  ?   �     1  -   O     }     �         (   -   f       I   F   Z       :   e       R   a           \              2   M   J   '   1   "   `              ?          !           <   /   O               &   D      m       4               %   
                 Y   @   A   5   ,      c   l   U       C   [                       >   d   7          E   N          S       9   ;   j   +   i   K   n       W      ]   0   V         L      X   k       h   B       b      Q   o      $   )       H   8       _   G      P      	   .          *      ^             g   T             #   3   6       =               %1$s on %2$s %s (Edit) ,  ... Read more &raquo; <h4>Related Posts</h4> <span id="footer-thankyou">Developed by <a href="#" target="_blank">Your Site Name</a></span>. Add New Add New Custom Category Add New Custom Tag Add New Custom Type Add New Project Category Add new Add new project All Custom Categories All Custom Posts All Custom Tags All Project Categories Avbryt Close Comment Custom Categories Custom Category Custom Post Custom RSS Feed (Customize in admin.php) Custom Tag Custom Tags Custom Types Edit Edit Custom Category Edit Custom Tag Edit Post Types Edit Project Category Email En kommentar Epic 404 - Article Not Found F Y First Footer Footer Links Inloggad som <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Logga ut?</a> Kommentera Kommentera på %s Last Menu Name New New Custom Category Name New Custom Tag Name New Post Type New Project Category Name Next Nothing found Nothing found in Trash Nothing found in the Database. Nothing found in trash Oops, Post Not Found! Parent Custom Category Parent Custom Category: Parent Custom Tag Parent Custom Tag: Parent Project Category Parent Project Category: Permalink to %s Posts by %s Previous Project Categories Project Category Project item Projects Read Read more >> Read more... Search Search Custom Categories Search Custom Tags Search Post Type Search Project Categories Search Results for: Search for: Search project Search... Show all Sidebar Sorry, No Results. Sticky Tags: The Footer. The Main Menu The RSS Feed is either empty or unavailable. The Top-bar. The article you were looking for was not found, but maybe try looking again! The first (primary) sidebar. This is the error message in the parts/content-missing.php template. This is the example custom post type Top-bar Try your search again. Uh Oh. Something is missing. Try double checking things. Update Custom Category Update Custom Tag Update Project Category View Post Type View all posts by %s View project Y You must be <a href="%s">logged in</a> to post a comment. Your comment is awaiting moderation. Your email address will not be published. j F Y @ g:i a project-slug Project-Id-Version: jointstheme
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-08-14 13:03+0200
PO-Revision-Date: 
Last-Translator: Mathias Hellquist <mathias.hellquist@gmail.com>
Language-Team: Ebeling Webbyrå
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n;esc_attr_x;_x;_nx
X-Poedit-Basepath: ../..
X-Generator: Poedit 2.1.1
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: node_modules
 %1$s på %2$s %s (Redigera) , Läs mera &raquo; <h4>Relaterade inlägg</h4> <span id="footer-thankyou">Utvecklat av <a href="#" target="_blank">Ebeling Webbyrå</a></span>. Lägg till Lägg till ny egen kategori Lägg till ny egen tagg Lägg till ny inläggstyp Lägg till ny projektkategori Lägg till Lägg till nytt projekt Alla egna kategorier Alla specialinlägg Alla egna taggar Alla projektkategorier Avbryt Stäng Kommentera Egna kategorier Egen kategori Specialinlägg Anpassat RSS flöde Egen tagg Special taggar Special inläggstyp Editera Editera egen kategori Editera egen tagg Editera inläggstyper Redigera projektkategori E-post En kommentar på &ldquo;%2$s&rdquo; Episk 404 - Sidan kan inte hittas F Y Första Footer-yta Länkar i footern Inloggad som <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Logga ut?</a> Kommentera Kommentera på %s Sista Meny Namn Ny Namn på ny egen kategori Nytt namn på egen tagg Ny inläggstyp Namn på ny projektkategori Nästa Ingenting kan hittas i papperskorgen Ingenting kan hittas i papperskorgen Ingenting kan hittas i databasen. Ingenting kan hittas i papperskorgen Kan inte hittas! Nedärvd egen kategori Nedärvd egen kategori: Nedärvd egen tagg Nedärvd egen tagg: Moder projektkategorier projektkategorier: Permalänk till %s Inlägg av %s Tidigare Kategoriserade inlägg: Projektkategori Projekt Projekt Läs mera Läs mer >> Läs mer... Sök Sök i de egna kategorierna Sök egna taggar Sök inläggstyp Sök i projektkategorierna Sök efter resultat för: Sök efter: Sök efter: Sök på sajten... Visa alla Sidomeny Tyvärr, inga resultat. Sticky Taggar: Footer-ytan. Huvudmeny RSS flödet är tom eller ej tillgängligt. Top-bar:en Inlägget du letade efter kan inte hittas. Försök igen! Primära sidomenyn. Felmeddelande från parts/content-missing.php template. Det här är ett exempel på inläggstyp Top-bar Prova sök igen. Uh Oh. Något gick fel. Prova att dubbelkolla allt igen. Uppdatera egen kategori Uppdatera egen tagg Uppdatera projektkategori Se inläggstyp Inlägg av %s Se projekt Y Du behöver vara <a href="%s">inloggad</a> för att kommentera. Din kommentar blir modererad. Din e-post adress kommer inte att publiceras. j F Y @ g:i a project-slug 