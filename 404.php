<?php get_header(); ?>
			
	<div id="content">

		<div class="inner">

		    <main id="main" role="main">

				<article id="content-not-found">
				
					<header class="article-header">
						<h1><?php _e( 'Epic 404 - Article Not Found', 'webbeling' ); ?></h1>
					</header> <!-- end article header -->
			
					<section class="entry-content">
						<p><?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'webbeling' ); ?></p>
						<p><?php _e( 'Go to one of our pages', 'webbeling' ); ?></p>
					</section> <!-- end article section -->

					<nav id="menu-404">
						<?php we_main_nav(); ?>
					</nav>
			
				</article> <!-- end article -->
	
			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>