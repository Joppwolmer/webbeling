"use strict";

var app = {
  name: "Webbeling",
  init: function () {
    We.pages.addToQueue("all-pages", function () {
      app.components.blocks.init();
      app.components.chat();
      app.components.logo();
      app.navigation.init();
      app.cart.init();
    });

    We.pages.addToQueue("start-page", function () { });

    We.pages.addToQueue("standard-page", function () { });

    We.pages.addToQueue("category-page", function () { });

    We.pages.addToQueue("product-page", function () { });
  },
  components: {
    logo: function () {
      if ($(".custom-logo-link img").length) {
        $(".custom-logo-link img").addClass("standard");
        var src = $(".custom-logo-link img").attr("src");
        var index = src.search(".png");
        var new_src = src.slice(0, index) + "-1" + src.slice(index);
        $(".custom-logo-link").append(
          $('<img class="white-logo" alt="Logo" />').attr("src", new_src)
        );
      }
    },
    chat: function () {
      $('.chat-activate').click(function () {
        log($('#maximizeChat'))
        document.getElementById("maximizeChat").click();
        $('#maximizeChat').click();
        return false;
      })
    },
    blocks: {
      init: function () {
        //WAIT FOR LOAD
        $(".wp-block-media-text").each(function () {
          app.components.blocks.checkHeight(this);
        });

        //WAIT FOR LOAD
        $(".wp-block-media-text img").on("load", function (event) {
          app.components.blocks.checkHeight(
            $(this).closest(".wp-block-media-text")
          );
        });
      },
      checkHeight: function (item) {
        var $content = $(item).find(".wp-block-media-text__content");
        var $media = $(item).find(".wp-block-media-text__media");
        $media.removeClass("crop");
        if ($content.outerHeight() > $media.find("img").outerHeight())
          $media.addClass("crop");
      }
    }
  },
  cart: {
    init: function () {
      $("#cart-button").click(function () {
        $("html").toggleClass("show-mini-cart");
        return false;
      });

      $("#overlay-background, #mini-cart-close").click(function () {
        $("html").removeClass("show-mini-cart");
      });
    }
  },
  navigation: {
    init: function () {
      app.navigation.mobile.init();
      app.navigation.desktop.init();
    },
    mobile: {
      init: function () {
        app.navigation.mobile.organize();
        app.navigation.mobile.events();
        app.navigation.mobile.hoverImage.init();
      },
      organize: function () {
        $("#nav-mobile li.menu-item-has-children").each(function () {
          $(this)
            .find(">ul")
            .prepend(
              $('<li class="show-all" />').html(
                $(this)
                  .find(">a")
                  .clone()
                  .html("Visa allt")
              )
            );
        });
      },
      hoverImage: {
        init: function () {
          //SPECIAL HOVER STYLE
          if ($("#nav-left-widget img").length > 1) {
            app.navigation.hoverImage.showActive(
              "#nav-mobile ul.menu>li.active"
            );
            $("#nav-mobile ul.menu>li").hover(function (index) {
              app.navigation.hoverImage.showActive(this);
            });
          }
        },
        showActive: function (element) {
          $("#nav-left-widget .widget_media_image").hide();
          var index = $(element).index() + 1;
          if (index === 0) index = 1;
          $(
            "#nav-left-widget .widget_media_image:nth-of-type(" + index + ")"
          ).show();
        }
      },
      events: function () {
        //MOBILE MENU
        $("#nav-mobile li.menu-item-has-children>a").click(function () {
          $(this)
            .parent()
            .toggleClass("open");
          return false;
        });

        $("#menu-icon").click(function () {
          $("#nav-mobile, html").addClass("nav-mobile-open");
        });

        $("#close-icon, #overlay-background").click(function () {
          $("#nav-mobile, html").removeClass("nav-mobile-open");
        });
      }
    },
    desktop: {
      init: function () {
        app.navigation.desktop.organize();
        app.navigation.desktop.events();
      },
      organize: function () {
        $("#nav-desktop li.menu-item-has-children").each(function () {
          $(this).append(
            $('<div class="sub-nav-wrapper" />')
              .append(
                $(this)
                  .find(">a")
                  .clone()
                  .addClass("sub-title")
              )
              .append(
                $(this)
                  .find(">ul")
                  .show()
              )
          );
        });
      },
      events: function () {
        //DESKTOP MENUs
        $("#nav-desktop li.menu-item-has-children").hover(function () {
          if (
            $(this)
              .hasClass("nav-desktop-open")
          ) {
            $(this)
              .removeClass("nav-desktop-open");
          } else {
            $("#nav-desktop li.menu-item-has-children").removeClass(
              "nav-desktop-open"
            );
            $(this)
              .addClass("nav-desktop-open");
          }
          return false;
        });

        //REMOVE IF CLICKED ELSEWHERE
        $("html").click(function () {
          if (typeof event != "undefined") {
            if (
              !$(event.target).closest("#nav-desktop li.menu-item-has-children")
                .length
            ) {
              if (
                $("#nav-desktop li.menu-item-has-children.nav-desktop-open")
                  .length
              ) {
                $("#nav-desktop li").removeClass("nav-desktop-open");
              }
            }
          }
        });
      }
    }
  }
};

We.pages.addToQueue("all-pages", app.init);
