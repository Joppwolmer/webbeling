/* jshint -W057 */
/* jshint -W058 */

//
//  "We"
//

var We = new function () {
    "use strict";

    this.name = "Webbeling WP Framework";
    this.version = "0.9.1";
    this.data = {
        currency: false,
        culture: false,
        language: false
    };
    this.checker = {
        isStartPage: false,
        isBlogPage: false,
        isSearchResultPage: false,
        isStandardPage: false
    };
    this.translations = [
        {
            cart: {
                sv: "Kundvagn",
                en: "Cart",
                da: "Indkøbskurv",
                nb: "Handlevogn",
                fi: "Ostoskärry"
            }
        }
    ];
    this.translate = function (translationObject) {
        for (var key in We.translations) {
            if (translationObject in We.translations[key]) {
                if (typeof We.translations[key][translationObject][We.data.language] !== "undefined") {
                    return We.translations[key][translationObject][We.data.language];
                }
                else if (typeof We.translations[key][translationObject]["en"] !== "undefined") {
                    console.log("No translation found for '" + translationObject + "' in language " + We.data.language + ". Returning English.");
                    return We.translations[key][translationObject]["en"];
                }
                else if (typeof We.translations[key][translationObject]["sv"] !== "undefined") {
                    console.log("No translation found for '" + translationObject + "' in language " + We.data.language + ". Returning Swedish.");
                    return We.translations[key][translationObject]["sv"];
                } else {
                    console.log("No translation found for '" + translationObject + "' in language " + We.data.language);
                }
            }
        }
        console.log("No translation found for " + translationObject + "'");
        return false;
    };
    this.setPageType = function () {
        if ($("body.home").length) {
            this.checker.isStartPage = true;
            return "start-page";
        } else if ($("body.blog").length > 0) {
            this.checker.isNewsPage = true;
            return "blog-page";
        } else if ($("body.page-showsearchresult").length > 0) {
            this.checker.isSearchResultPage = true;
            return "searchresult-page";
        } else if ($("body.single-product.woocommerce").length > 0) {
            this.checker.isProductPage = true;
            return "product-page";
        } else if ($("body.woocommerce-page:not(.single-product)").length > 0) {
            this.checker.isCategoryPage = true;
            return "category-page";
        } else {
            this.checker.isStandardPage = true;
            return "standard-page";
        }
    };
    this.components = {
        browserDetect: function () {
            We.browser = bowser;
            addBodyClass("browser-" + convertStringToURLFriendly(bowser.name));
            if (bowser.msie) {
                addBodyClass("msie");
                if (bowser.version == 7) addBodyClass("ie7 msie-old");
                if (bowser.version == 8) addBodyClass("ie8 msie-old");
                if (bowser.version == 9) addBodyClass("ie9");
                if (bowser.version == 10) addBodyClass("ie10");
                if (bowser.version == 11) addBodyClass("ie11");
            } else {
                addBodyClass("not-msie");
            }
            if (bowser.webkit) addBodyClass("webkit");
            if (bowser.blink) addBodyClass("blink");
            if (bowser.gecko) addBodyClass("gecko");
            if (bowser.ios) addBodyClass("ios");
            if (bowser.android) addBodyClass("android");
            if (bowser.mobile) addBodyClass("mobile");
            if (bowser.tablet) addBodyClass("tablet");
            if (bowser.tablet || bowser.mobile || ('ontouchstart' in window)) We.checker.isTouch = true;
        },
        registerHandlebarHelpers: function () {
            Handlebars.registerHelper('translate', function (obj) {
                var str = arguments[0];
                return Eb.translate(str);
            });
            Handlebars.registerHelper("ifValue", function (conditional, options) {
                if (conditional == options.hash.equals) {
                    return options.fn(this);
                } else {
                    return options.inverse(this);
                }
            });
            Handlebars.registerHelper("replace", function (str, a, b) {
                // Example: {{{replace ImageUrl.Url "small" "medium"}}}
                if (str && typeof str === 'string') {
                    if (!a || typeof a !== 'string') return str;
                    if (!b || typeof b !== 'string') b = '';
                    return str.split(a).join(b);
                }
            });
            Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
                switch (operator) {
                    case '==':
                        return (v1 == v2) ? options.fn(this) : options.inverse(this);
                    case '===':
                        return (v1 === v2) ? options.fn(this) : options.inverse(this);
                    case '!=':
                        return (v1 != v2) ? options.fn(this) : options.inverse(this);
                    case '!==':
                        return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                    case '<':
                        return (v1 < v2) ? options.fn(this) : options.inverse(this);
                    case '<=':
                        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                    case '>':
                        return (v1 > v2) ? options.fn(this) : options.inverse(this);
                    case '>=':
                        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                    case '&&':
                        return (v1 && v2) ? options.fn(this) : options.inverse(this);
                    case '||':
                        return (v1 || v2) ? options.fn(this) : options.inverse(this);
                    default:
                        return options.inverse(this);
                }
            });
            Handlebars.registerHelper('classFriendly', function(v1) {
                var str = arguments[0];
                //Lower case everything
                str = str.toLowerCase();
                //Convert whitespaces and underscore to dash
                str = str.replace(/\s/g, "-");
                return str;
            });
        },
        scrollEvent: function(){

            //SCROLLING EVENTS
            var scroll;
            var scrollUp;
            var lastScrollTop = 0;

            $(window).scroll(function(event){
                if($(window).scrollTop() > 50)
                    scroll = true;
                else{
                    scroll = false;
                }
                var st = $(this).scrollTop();
                if (st > lastScrollTop){
                    scrollUp = false;
                } else {
                    scrollUp = true;
                }
                lastScrollTop = st;
            });

            setInterval(function() {
                if (scroll) {
                    $("html").addClass("scrolling");
                }else{
                    $("html").removeClass("scrolling");
                }
                if (scrollUp) {
                    $("html").addClass("up");
                }else{
                    $("html").removeClass("up");
                }
            }, 200);

        },

    };
    this.pages = {
        allPages: function () {

        },
        queues: {
            "all-pages": [],
            "start-page": [],
            "product-page": [],
            "category-advanced-page": [],
            "category-page": [],
            "orderconfirmation-page": [],
            "manufacturer-advanced-page": [],
            "manufacturer-page": [],
            "news-page": [],
            "checkout-page": [],
            "searchresult-page": [],
            "my-page": [],
            "changepassword-page": [],
            "standard-page": [],
            "sitemap-page": []
        },
        addToQueue: function (pageType, queueItem) {
            if (typeof We.pages.queues[pageType] != "undefined") {
                We.pages.queues[pageType].push(queueItem);
            } else {
                console.log("Error: Queue-type not found: " + pageType);
            }
        },
        init: function () {
            We.pages.allPages();
            executeQueue(We.pages.queues["all-pages"]);
            if (typeof We.pages.queues[We.pageType] != "undefined") {
                executeQueue(We.pages.queues[We.pageType]);
            } else {
                console.log("Error: Queue-type not found: " + We.pageType);
            }
        }
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Method calls, init and setup
    //

    this.pageType = this.setPageType();
    //
    //  Initialize
    //
    this.init = function () {
        addBodyClass(this.pageType);
        We.components.registerHandlebarHelpers();
        We.components.browserDetect();
        We.components.scrollEvent();
        We.pages.init();

        // Prevent J.init function from firing twice
        We.init = function () {
            console.error("We.init() triggered twice. Check base.master & js management!");
        };
    };
};

$(document).ready(function () {
    We.init();
});

