var weChart = {
  dataList: [],
  init: function () {
    $(".chart-container").each(function (index) {
      $(this).attr("data-id", index);
      var element = ".chart-container[data-id=" + index + "]";
      var data = weChart.getData(element);
      weChart.render(data, element);
    });
  },
  render: function (data, element) {
    var template = WeViews["we-chart/we-chart"];
    var html = template(data);
    $(element).html(html);
    weChart.show(element);
    weChart.events(element);
  },
  getData: function (element) {
    var data = {
      head: {
        title: $(element).data("title"),
        min: $(element).data("min"),
        max: $(element).data("max"),
      },
      list: [],
    };

    $(element)
      .find("dd")
      .each(function (index) {
        var item = {
          title: $(this).data("title"),
          value: $(this).data("value"),
          text: $(this).data("text"),
          index: index,
        };
        data.list.push(item);
      });
    return data;
  },
  show: function (element) {
    $(element).css("opacity", "1").find(".chart-item[data-index='0']");
    weChart.selected(element, $(element).find(".chart-item[data-index='0']"));
  },
  events: function (element) {
    $(element)
      .find(".chart-item")
      .hover(function () {
        weChart.selected(element, this);
      });
  },
  selected: function (element, item) {
    $(element).find(".chart-item").removeClass("selected");
    $(item).addClass("selected");
    $(element).find(".chart-title").html($(item).attr("data-text"));
    //weContent.blocks.init();
  },
};

We.pages.addToQueue("all-pages", weChart.init);
