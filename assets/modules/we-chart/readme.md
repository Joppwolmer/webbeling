

/*********************
IMAGE BOX
*********************/

//CHART CONTAINER
<div class="chart-container" data-min="0" data-max="0" data-title="Title of graph">
    <dd data-title="Känsla" data-value="6" data-text="Vi drivs av att få arbeta med vår kreativa kraft. Skapande är vårt drivmedel, rikta in vårt sikte mot ditt mål - så tar vi dig dit." />
    <dd data-title="React" data-value="7" data-text="Arbeta med vår kreativa kraft. Skapande är vårt drivmedel, rikta in vårt sikte mot ditt mål - så tar vi dig dit." />
    <dd data-title="Lugn" data-value="2" data-text="Skapande är vårt drivmedel, rikta in vårt sikte mot ditt mål - så tar vi dig dit." />
    <dd data-title="Beslutsamhet" data-value="9" data-text="Med vår kreativa kraft. Skapande är vårt drivmedel, rikta in vårt sikte mot ditt mål - så tar vi dig dit." />
</div>
