var instaFeed = {
    init: function () {
        if ($("#instagram-feed").length > 0)
            instaFeed.render($("#instagram-feed").attr('data-user-id'), $("#instagram-feed").attr('data-access-token'));
    },
    render: function (userId, accessToken) {
        $("#instagram-feed").append($('<ul id="instafeed" />'));
        var feed = new Instafeed({
            get: "user",
            userId: userId,
            tagName: "",
            accessToken: accessToken,
            template: '<li><a target="_blank" href="{{link}}"><img src="{{image}}" /><div class="info"><span class="likes">{{likes}}</span><span class="comments">{{comments}}</span><span class="location">{{location}}</span></div></a></li>',
            limit: 12,
            resolution: "standard_resolution", //thumbnail low_resolution standard_resolution
            after: instaFeed.after
        });
        feed.run();
    },
    after: function(){
      $('#instafeed li').each(function(){
          if($(this).find('.location').html().length === 0)
              $(this).find('.location').remove();
      })
    }
}

We.pages.addToQueue("all-pages", instaFeed.init);
