var weLandingPage = {
  settings: {
    imageDelay: 700,
    cookieName: "landingPageShown",
    active: true
  },
  load: function(){
    if(getCookie(weLandingPage.settings.cookieName)){
      weLandingPage.settings.active = false;
      weLandingPage.showSite();
    }
  },
  init: function() {
    if(weLandingPage.settings.active){
      $("#landing-html").show();
      weLandingPage.generate();
    }
  },
  generate: function(){
    setCookie(weLandingPage.settings.cookieName, true);
    weLandingPage.getData();
    weLandingPage.showSite();
    setTimeout(function(){
      weLandingPage.render();
    }, weLandingPage.settings.imageDelay*1.5);
  },
  render: function(){
    $($("#landing-images .landing-image").get().reverse()).each(function(i){
      var $that = $(this);
      var timer = (weLandingPage.settings.imageDelay-(i*40))*i;

      (function (i) {
        setTimeout(function () {
          if(i === $("#landing-images .landing-image").length-1){
            $("#landing-html").animate({
              left: '100%'
            },function(){
              $("#landing-html").remove()
            });
          }else if(i%2 === 0)
            $that.animate({
              height: '0'
            });
          else
            $that.animate({
              width: '0'
            });
        }, timer);
      })(i);
    })
  },
  getData: function(){   
    //EACH IMAGE
    $($("#landing-images img").get().reverse()).each(function(){
      $("#landing-html #landing-images").append($('<div class="landing-image" />').css('background-image', 'url(' + $(this).attr('src')+ ')'));
      $(this).remove();
    });

      //BLANK SPACE
      $("#landing-html #landing-images").append($('<div class="landing-image" />'));
  },
  showSite: function(){
    $('body').addClass('show');
  }
};

weLandingPage.load();

We.pages.addToQueue("start-page", weLandingPage.init);
