var weProductPage = {
    init: function(){

            //INIT SLIDER
        weProductPage.slider();

        //VARIATIONS FIX
        //weProductPage.productPage.variations();

          //CREATES BUTTON FROM SELECT ELEMENT
        weProductPage.variationButtons.init();

          //MOVING HTML ELEMENTS
        weProductPage.underProductButton();

          // SCROLL FROM DECRIPTION TO BUY BUTTON
        weProductPage.scrollButtonProductPage();

          // SIZE GUIDE ON PRODUCT PAGE
        weProductPage.sizeGuide();

    },
    slider: function(){

        if($('.woocommerce-product-gallery a').length>1) {

            //CONVERTING HTML TO OWL SLICK SLIDER HTML
            var $slider_container = $('<div id="product-slider" />');

            $('.woocommerce-product-gallery a').each(function () {

                var $oldImg = $(this).find('img');

                var $link = $('<a />');
                $link.attr('href', $(this).attr('href'));
                $link.attr('data-rel', $(this).attr('data-rel'));
                $link.attr('class', $(this).attr('class'));
                $link.attr('data-fancybox', 'group');

                var $img = $('<img />');
                var srcSet = $oldImg.attr('srcset').split(",");
                var newSrc = srcSet[srcSet.length - 2].split(" ")[1];
                var newSrc = $oldImg.attr('src');

                $img.attr('src', newSrc);

                var $item = $('<div class="item" />').append($link.append($img));
                $slider_container.append($item);
            });

            $('div.images').append($slider_container);

            $('div.images>figure').remove();

            $("#product-slider").slick({
                dots: true, //MUST BE TRUE TO WORK WITH THUMBS (HIDDEN IN CSS)
                slidesToShow: 1,
                arrows: false,
                responsive: [{
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 1,
                    },
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                    },
                }],
            });
        }
    },
    variations: function(){
        if($('input[name="variation_id"]').length){
            setTimeout(function(){
                $('.single_variation_wrap').css("display", "block");
            }, 1);

            $('.single_add_to_cart_button').on("click", function(){
                if(!$('input[name="variation_id"]').val()){
                    $('.variations select').addClass('error');
                    return false;
                }
            });
            $('.variations select').on("click",function(){
                $(this).removeClass('error');
            });
        }
    },
    underProductButton: function(){
        $('.single-product div.product .summary').append($('.product-usp').clone());
        $('.single-product div.product .summary').append($('.under-product-button'));
    },
    variationButtons: {
        init: function(){
            var $wrapper = $('<ul id="variation-button" />');

            //LOOP EACH OPTION
            $('.variations select option').each(function(){
                var $item = $('<a class="item" href="#" />');
                $item
                    .html($(this).html())
                    .attr('class', $(this).attr('class'))
                    .attr('data-value', $(this).attr('value'));
                $wrapper.append($('<li />').append($item));
            });

            //ADD TO DOM AND HIDE
            $('table.variations').after($wrapper);
            $('.variations .value').hide();

            //EVENTS
            weProductPage.variationButtons.events();

        },
        events: function(){
            $('#variation-button a').click(function(){
                $('#variation-button a').removeClass('selected');
                $(this).addClass('selected');
                $('.variations select').val($(this).attr('data-value')).trigger('change');
                return false;
            });
        }
    },
    scrollButtonProductPage: function() {
        $('.woocommerce-Tabs-panel--description .product-scroll-button').click(function(){
            $('html, body').animate({
                scrollTop: $(".single_add_to_cart_button").offset().top - 400
            }, 1000);
        });
    },
    sizeGuide: function() {
        $(".variations_form td.label").append($('#size-guide'));
    }
};

We.pages.addToQueue("product-page", weProductPage.init);