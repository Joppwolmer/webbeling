var weProject = {
  init: function() {
    if (
      $("body").hasClass("post-type-archive-project") ||
      $("body").hasClass("tax-project_cat")
    ) {
      log("astar");
      weProject.events();
      weProject.active();
    }

    if ($("body").hasClass("single-project")) {
      weProject.singlePage();
    }
  },
  active: function() {
    if ($(".category-list li.current-cat").length)
      $(".category-list li.current-cat a").click();
  },
  events: function() {
    //CATEGORY CHANGE
    weProject.list = $(".project-list>article");

    $("ul.category-list a").click(function() {
      // HANDLE SELECTED
      $(this)
        .closest(".category-list")
        .find("a")
        .removeClass("selected");
      $(this).toggleClass("selected");
      var className = $(this)
        .html()
        .replace(/[^a-z0-9]/gi, "-");
      var link = this;

      log(className);

      $(".project-list>article").remove();

      $(weProject.list).each(function() {
        if (
          $(this).hasClass("project_cat-" + className.toLowerCase()) ||
          $(link).hasClass("show-all")
        ) {
          $(".project-container>.project-list").append($(this));
        }
      });

      return false;
    });

    var value = $("body.post-type-archive-project #main #categoryGet").val();
    if (value) $("ul.category-list a:contains('" + value + "')").click();
  },
  singlePage: function() {}
};

We.pages.addToQueue("standard-page", weProject.init);
