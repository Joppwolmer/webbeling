<header class="page-header-full-wrapper<?php if ( has_post_thumbnail() ) { ?> has-image<?php } ?>">
     <div class="page-header-inner">
        <?php if ( has_post_thumbnail() ) { ?>
            <div class="page-header-image">
                <?php the_post_thumbnail('full'); ?>
             </div>
         <?php } ?>
         <div class="page-header-title">
            <h1 class="page-title"><?php the_title(); ?></h1>
         </div>
     </div>
</header>