<header class="page-header-wrapper">
    <?php if ( has_post_thumbnail() ) { ?>
        <div class="page-header-image">
            <?php the_post_thumbnail('full'); ?>
            </div>
        <?php } ?>
        <div class="page-header-title">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
</header>