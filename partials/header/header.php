<?php if ( is_active_sidebar( 'top-bar' ) ) : ?>
    <?php get_template_part( 'partials/header/top', 'bar' ); ?>
<?php endif; ?>
<header id="header">
    <div class="inner">
        <div class="header-inner">
            <div class="left-side">
                <a id="menu-icon" class="header-icon hide-for-large">
                    <span class="icon"></span>
                    <span class="text"><?php _e( 'Menu', 'webbeling' ); ?></span>
                </a>
            </div>
            <?php the_custom_logo(); ?>
            <?php get_template_part( 'partials/header/nav', 'desktop' ); ?>
            <div class="right-side">
                <?php header_cart(); ?>
            </div>
        </div>
    </div>
</header>
<?php get_template_part( 'partials/header/nav', 'mobile' ); ?>
<?php mini_cart(); ?>
