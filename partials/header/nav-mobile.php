<nav id="nav-mobile">
    <div class="nav-mobile-inner <?php if ( is_active_sidebar( 'nav-left' ) ) : ?>left-active<?php endif; ?>">
        <?php if ( is_active_sidebar( 'nav-left' ) ) : ?>
            <div class="nav-secondary">
                <div id="nav-left-widget" role="complementary">
                    <?php dynamic_sidebar( 'nav-left' ); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="nav-primary">
            <div id="nav-mobile-header">
                <div id="nav-logo"><?php the_custom_logo(); ?></div>
                <a id="close-icon" class="header-icon">
                    <span class="icon"></span>
                    <span class="text"><?php _e( 'Close', 'webbeling' ); ?></span>
                </a>
            </div>
            <?php we_main_nav(); ?>
            <div id="nav-right-widget" role="complementary">
                <?php if ( is_active_sidebar( 'nav-right' ) ) : ?>
                    <?php dynamic_sidebar( 'nav-right' ); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</nav>