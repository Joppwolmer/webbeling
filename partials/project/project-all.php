<header class="page-header-full-wrapper">
     <div class="page-header-inner">
         <div class="page-header-title">
            <h1 class="page-title"><?php wp_title('', true,''); ?></h1>
         </div>
     </div>
</header>

<div class="full-screen">

    <main id="main" role="main">

        <ul class="category-list">
        <li><a href="#" class="show-all selected"><?php _e('Show all', 'webbeling') ?></a></li>
            <?php
            $type = $_GET['typ'];
            $customPostTaxonomies = get_object_taxonomies('project');
            if(count($customPostTaxonomies) > 0)
            {
                foreach($customPostTaxonomies as $tax)
                {
                    $args = array(
                        'orderby' => 'name',
                        'show_count' => 0,
                        'pad_counts' => 0,
                        'hierarchical' => 1,
                        'taxonomy' => $tax,
                        'title_li' => ''
                        );

                    wp_list_categories( $args );
                }
            }?>
        </ul>
        <input type="hidden" id="categoryGet" value="<?php echo $type; ?>" />
        <?php
        $args = array(
        'post_type' => 'project',
        'posts_per_page' => -1,
        );
        $portfolio = new WP_Query( $args );

        if( $portfolio->have_posts() ) { ?>
            <div class='project-container'>
                <div class="project-list">
                    <?php while( $portfolio->have_posts() ) {?>
                        <?php $portfolio->the_post(); ?>
                        <?php get_template_part( 'partials/project/project', 'item' ); ?>
                    <?php } ?>
                </ul>
            </div>
        <?php } else { ?>

            <?php get_template_part( 'partials/content', 'missing' ); ?>

        <?php } ?>

    </main> <!-- end #main -->

</div> <!-- end #inner-content -->


