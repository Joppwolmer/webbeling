<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
    <div class="project-inner">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class='project-featured-img-link'>
            <?php
            if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                the_post_thumbnail('project-featured-img');
            }else{
                echo "<img class='attachment-project-featured-img' src='". plugins_url( '/ebeling-portfolio/library/images/thumbnail.png') ."' alt='thumbnail' />";
            }
            ?>
            <span class="project-magnify"></span>
        </a>
        <div class="project-text">
            <h3 class='project-title'><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <div class="category-links">
                <?php
                $list = get_the_terms(get_the_ID(), 'project_cat');
                $url = get_site_url();
                foreach($list as $item){
                    echo "<a href='$url/projekt?typ=$item->name'>$item->name</a>";
                }
                ?>
            </div>
            <div class='project-preview'>
                <?php
                    $preview = get_post_meta( get_the_ID(), "project-preview", true );
                    echo "<p>" . $preview . "</p>";
                ?>
            </div>
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="button overlay"><?php _e("Read more") ?></a>
        </div>
    </div>
</article> <!-- end article -->