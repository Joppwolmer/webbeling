<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    <header class="project-header">
        <div class="project-header-inner">
            <div class="header-text">
                <div class="header-inner">
                    <h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
                    <span class="category-links">
                        <?php
                        $list = get_the_terms($postId, 'project_cat');
                        $url = get_site_url();
                        foreach($list as $item){
                            echo "<a href='$url/projekt?typ=$item->name'>$item->name</a>";
                        }
                        ?>
                    </span>
                </div>
            </div>
        </div>
    </header>
    <section class="project-content entry-content clearfix" itemprop="articleBody">
        <?php the_content(); ?>
    </section>
    <section class="project-meta clearfix" itemprop="articleBody">
        <?php
            $postId = get_the_ID();
            echo get_post_meta($postId, 'project_meta', true);
        ?>
    </section>
	<?php comments_template('/partials/comments.php'); ?>
    <div class="project-cta">
        <?php dynamic_sidebar( 'project_cta' ); ?>
    </div>
</article>