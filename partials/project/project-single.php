<div class="full-screen">

    <main id="main" role="main">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php get_template_part( 'partials/project/project', 'single-content' ); ?>

        <?php endwhile; else : ?>

            <?php get_template_part( 'partials/post/content', 'missing' ); ?>

        <?php endif; ?>

    </main> <!-- end #main -->

</div> <!-- end #inner-content -->