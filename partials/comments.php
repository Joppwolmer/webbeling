<?php
	if ( post_password_required() ) {
		return;
	}
?>
<div id="comments" class="comments-area">

	<?php // You can start editing here ?>

	<?php if ( have_comments() ) : ?>
        <h2 class="comments-title">
            <?php

            $count = get_comments_number();
            if(count === 0)
              esc_html_e( 'No comments', 'webbeling' );
            else if(count === 1)
              esc_html_e( 'One comment', 'webbeling' );
            else
            echo sprintf(
              __( '%1$s comments', 'webbeling' ),
              $count
            )
            ?>
        </h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'webbeling' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'webbeling' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'webbeling' ) ); ?></div>

			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-above -->
		<?php endif; // Check for comment navigation. ?>

		<ol class="commentlist">
			<?php wp_list_comments('type=comment&callback=joints_comments'); ?>
		</ol>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Kommentar navigation', 'webbeling' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'webbeling' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'webbeling' ) ); ?></div>

			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-below -->
		<?php endif; // Check for comment navigation. ?>

	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php esc_html_e( 'Comments closed.', 'webbeling' ); ?></p>
	<?php endif; ?>

	<?php
	$args = array(
      'id_form'           => 'commentform',
       'class_form'       => 'comment-form',
      'id_submit'         => 'submit',
      'class_submit'      => 'button',
      'name_submit'       => 'submit',
      'title_reply'       => __( 'Comment', 'webbeling' ),
      'title_reply_to'    => __( 'Comment on %s', 'webbeling' ),
      'cancel_reply_link' => __( 'Cancel', 'webbeling' ),
      'label_submit'      => __( 'Comment', 'webbeling' ),
      'format'            => 'xhtml',

      'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . __( 'Comment', 'webbeling' ) .
        '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true">' .
        '</textarea></p>',

      'must_log_in' => '<p class="must-log-in">' .
        sprintf(
          __( 'You must be <a href="%s">logged in</a> to post a comment.', 'webbeling' ),
          wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
        ) . '</p>',

      'logged_in_as' => '<p class="logged-in-as">' .
        sprintf(
        __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'webbeling' ),
          admin_url( 'profile.php' ),
          $user_identity,
          wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
        ) . '</p>',

      'comment_notes_before' => '<p class="comment-notes">' .
        __( 'Your email address will not be published.', 'webbeling' ) . ( $req ? $required_text : '' ) .
        '</p>',

      'comment_notes_after' => "",

      'fields' => array(

        'author' =>
          '<p class="comment-form-author"><label for="author">' . __( 'Name', 'domainreference' ) .
          ( $req ? '<span class="required">*</span>' : '' ) . '</label>' .
          '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
          '" size="30"' . $aria_req . ' /></p>',

        'email' =>
          '<p class="comment-form-email"><label for="email">' . __( 'Email', 'webbeling' ) .
          ( $req ? '<span class="required">*</span>' : '' ) . '</label>' .
          '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
          '" size="30"' . $aria_req . ' /></p>'
      ),
    );
	comment_form($args); ?>

</div><!-- #comments -->