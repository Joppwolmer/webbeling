<footer id="footer" role="contentinfo">
    <div id="footer-main">
        <div class="inner">
            <div id="footer-widget" role="complementary">
                <?php dynamic_sidebar( 'footer' ); ?>
            </div>
        </div>
    </div>
    <div id="footer-bottom" role="contentinfo">
        <div class="inner">
            <div id="footer-bottom-widget" role="complementary">
                <?php dynamic_sidebar( 'footer-bottom' ); ?>
            </div>
        </div>
    </div>
</footer>