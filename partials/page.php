<?php get_template_part( 'partials/page', 'header-full' ); ?>

<div class="inner">

    <main id="main" role="main">

        <?php get_template_part( 'partials/page', 'content' ); ?>

    </main> <!-- end #main -->

</div> <!-- end #inner-content -->