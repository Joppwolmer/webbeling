<header class="page-header-full-wrapper">
    <div class="page-header-inner">
        <div class="page-header-title">
            <h1 class="page-title"><?php wp_title('', true,''); ?></h1>
        </div>
    </div>
</header>

<div class="inner">

    <main id="main" role="main">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <!-- To see additional archive styles, visit the /parts directory -->
            <?php get_template_part( 'partials/post/loop', 'archive' ); ?>

        <?php endwhile; ?>

            <?php joints_page_navi(); ?>

        <?php else : ?>

            <?php get_template_part( 'partials/post/content', 'missing' ); ?>

        <?php endif; ?>

    </main> <!-- end #main -->

</div> <!-- end #inner-content -->