<div class="inner">

    <main id="main" role="main">

        <header>
            <h1 class="page-title"><?php the_archive_title();?></h1>
            <?php the_archive_description('<div class="taxonomy-description">', '</div>');?>
        </header>

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <!-- To see additional archive styles, visit the /parts directory -->
            <?php get_template_part( 'partials/post/loop', 'archive' ); ?>

        <?php endwhile; ?>

            <?php joints_page_navi(); ?>

        <?php else : ?>

            <?php get_template_part( 'partials/post/content', 'missing' ); ?>

        <?php endif; ?>

    </main> <!-- end #main -->

    <?php get_sidebar(); ?>

</div> <!-- end #inner-content -->