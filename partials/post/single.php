<div class="inner">

    <main id="main" role="main">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php get_template_part( 'partials/post/loop', 'single' ); ?>

        <?php endwhile; else : ?>

            <?php get_template_part( 'partials/post/content', 'missing' ); ?>

        <?php endif; ?>

    </main> <!-- end #main -->

    <?php //get_sidebar(); ?>

</div> <!-- end #inner-content -->